package com.heydiezuliansyah.calculator.Controllers;

import com.heydiezuliansyah.calculator.Models.Calculator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CalculatorController {

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("operator", "+");
        model.addAttribute("view", "views/calculatorForm");
        return "baseLayout";
    }

    @PostMapping("/")
    public String index(
            @RequestParam String leftOperand,
            @RequestParam String rightOperand,
            @RequestParam String operator,
            Model model
    ) {
        double leftNumber;
        double rightNumber;

        try {
            leftNumber = Double.parseDouble(leftOperand);
        } catch (NumberFormatException e) {
            leftNumber = 0;
        }

        try {
            rightNumber = Double.parseDouble(rightOperand);
        } catch (NumberFormatException e) {
            rightNumber = 0;
        }

        Calculator calculator = new Calculator(
                leftNumber,
                rightNumber,
                operator
        );

        double result = calculator.calculateResult();
        model.addAttribute("leftOperand", leftNumber);
        model.addAttribute("rightOperand", rightNumber);
        model.addAttribute("operator", operator);
        model.addAttribute("result", result);

        model.addAttribute("view", "views/calculatorForm");

        return "baseLayout";
    }

}
